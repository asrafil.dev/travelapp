import 'package:flutter/material.dart';
import '/ui/shared/theme.dart';
import 'ui/pages/page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: mainTheme,
      routes: {
        '/': (context) => const SplashPage(),
        '/getstarted': (context) => const GetStarted(),
        '/signup': (context) => const SignUpPage(),
        '/bonus': (context) => const BonusPage(),
        '/home': (context) => const HomePage(),
      },
    );
  }
}
