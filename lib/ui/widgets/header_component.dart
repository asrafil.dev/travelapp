import 'package:flutter/material.dart';
import 'package:travelapp/ui/shared/theme.dart';

class CustomHeader extends StatelessWidget {
  const CustomHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Howdy,\nKezia Anne",
                style: h2TextStyle.copyWith(
                  color: blackColor,
                ),
              ),
              const SizedBox(height: 6),
              Text(
                "Where to Fly Today?",
                style: greyTextStyle.copyWith(
                  fontWeight: light,
                  fontSize: 16,
                ),
              ),
            ],
          ),
        ),
        Container(
          width: 60,
          height: 60,
          decoration: BoxDecoration(
            border: Border.all(
              color: whiteColor,
              width: 4,
            ),
            shape: BoxShape.circle,
            image: const DecorationImage(
              image: AssetImage("assets/profile_pic.png"),
            ),
          ),
        )
      ],
    );
  }
}
