import 'package:flutter/material.dart';
import 'package:travelapp/ui/shared/theme.dart';
import 'package:travelapp/ui/widgets/header_component.dart';
import 'package:travelapp/ui/widgets/main_destination_card_component.dart';
export 'package:flutter/widgets.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: EdgeInsets.all(defaultMargin),
      child: ListView(
        children: [
          CustomHeader(),
          MainDestinationCard(),
        ],
      ),
    ));
  }
}
