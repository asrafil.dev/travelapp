import 'package:flutter/material.dart';
import 'package:travelapp/ui/shared/theme.dart';

class BonusPage extends StatelessWidget {
  const BonusPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget bonusButton() {
      return SizedBox(
        height: 55,
        width: 220,
        child: ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, '/home');
          },
          style: primaryButton,
          child: Text(
            "Start Fly Now",
            style: whiteTextStyle.copyWith(
              fontSize: 18,
              fontWeight: medium,
            ),
          ),
        ),
      );
    }

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            card(),
            bonusInfo(),
            bonusButton(),
          ],
        ),
      ),
    );
  }
}

Widget bonusInfo() {
  return Container(
    margin: const EdgeInsets.only(
      top: 80,
      bottom: 50,
    ),
    child: Column(
      children: [
        Text(
          "Big Bonus 🎉",
          style: h2TextStyle.copyWith(
            color: blackColor,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          "We give you early credit so that\nyou can buy a flight ticket",
          style: greyTextStyle.copyWith(
            fontSize: 16,
            fontWeight: light,
          ),
          textAlign: TextAlign.center,
        )
      ],
    ),
  );
}

Widget card() {
  return Container(
    width: 300,
    height: 200,
    decoration: const BoxDecoration(
      image: DecorationImage(
        image: AssetImage("assets/card_bg.png"),
      ),
    ),
    child: Padding(
      padding: const EdgeInsets.all(defaultMargin),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          cardID(),
          cardBalance(),
        ],
      ),
    ),
  );
}

Widget cardID() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Name",
            style: whiteTextStyle.copyWith(
              fontWeight: light,
            ),
          ),
          Text(
            "Kezia Anne",
            style: whiteTextStyle.copyWith(
              fontSize: 20,
              fontWeight: medium,
            ),
          )
        ],
      ),
      Row(
        children: [
          Image.asset(
            "assets/logo.png",
            width: 24,
          ),
          const SizedBox(
            width: 6,
          ),
          Text(
            "Pay",
            style: whiteTextStyle.copyWith(
              fontSize: 16,
              fontWeight: medium,
            ),
          )
        ],
      ),
    ],
  );
}

Widget cardBalance() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        "Balance",
        style: whiteTextStyle.copyWith(
          fontWeight: light,
        ),
      ),
      Text(
        "IDR 280.000.000",
        style: whiteTextStyle.copyWith(
          fontSize: 26,
          fontWeight: medium,
        ),
      )
    ],
  );
}
