import 'package:flutter/material.dart';
import 'package:travelapp/ui/shared/theme.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget inputForm() {
      return Container(
        margin: const EdgeInsets.only(
          top: 30,
          bottom: 30,
        ),
        padding: const EdgeInsets.symmetric(
          horizontal: defaultMargin,
          vertical: 30.0,
        ),
        decoration: const BoxDecoration(
            color: whiteColor,
            borderRadius: BorderRadius.all(Radius.circular(defaultRadius))),
        child: Column(
          children: [
            inputComponent(
              inputTitle: "Full Name",
            ),
            inputComponent(
              inputTitle: "Email Address",
            ),
            inputComponent(
              inputTitle: "Password",
              obscuretext: true,
            ),
            inputComponent(
              inputTitle: "Hobby",
            ),
            const SizedBox(height: 10),
            SizedBox(
              height: 55,
              width: 287,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/bonus');
                },
                style: primaryButton,
                child: Text(
                  "Get Started",
                  style: whiteTextStyle.copyWith(
                    fontSize: 18,
                    fontWeight: medium,
                  ),
                ),
              ),
            )
          ],
        ),
      );
    }

    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(defaultMargin),
          child: ListView(
            children: [
              title(),
              inputForm(),
              termsButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget title() {
    return Text(
      "Join us and get\nyour next journey",
      style: h3TextStyle.copyWith(
        color: blackColor,
      ),
    );
  }

  Widget termsButton() {
    return TextButton(
      onPressed: () {},
      child: Text(
        "Terms and Conditions",
        style: greyTextStyle.copyWith(
          fontWeight: light,
          decoration: TextDecoration.underline,
        ),
      ),
    );
  }

  Widget inputComponent(
      {required String inputTitle, bool obscuretext = false}) {
    return Container(
      margin: const EdgeInsets.only(bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(inputTitle),
          const SizedBox(height: 6),
          TextFormField(
            cursorColor: blackColor,
            obscureText: obscuretext,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(defaultRadius),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(defaultRadius),
                borderSide: const BorderSide(
                  color: primaryColor,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
