import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

ThemeData mainTheme = ThemeData(
  fontFamily: GoogleFonts.poppins().fontFamily,
  scaffoldBackgroundColor: backgroundColor,
  primaryColor: primaryColor,
);

//COLORS
const primaryColor = Color(0xFF5C40CC);
const blackColor = Color(0xFF1F1449);
const whiteColor = Color(0xffFFFFFF);
const greyColor = Color(0xFF9698A9);
const backgroundColor = Color(0xFFFAFAFA);

//TEXTSTYLE
const TextStyle primaryTextStyle = TextStyle(color: primaryColor);
const TextStyle blackTextStyle = TextStyle(color: blackColor);
const TextStyle whiteTextStyle = TextStyle(color: whiteColor);
const TextStyle greyTextStyle = TextStyle(color: greyColor);

const TextStyle h2TextStyle = TextStyle(
  fontSize: 32,
  fontWeight: semibold,
);

const TextStyle h3TextStyle = TextStyle(
  fontSize: 24,
  fontWeight: semibold,
);

const TextStyle h4TextStyle = TextStyle(
  fontSize: 18,
  fontWeight: semibold,
);

//FONTWEIGHT
const FontWeight light = FontWeight.w300;
const FontWeight medium = FontWeight.w500;
const FontWeight semibold = FontWeight.w600;
const FontWeight bold = FontWeight.w700;
const FontWeight extrabold = FontWeight.w800;

//DEFAULTSIZE

const double defaultMargin = 24.0;
const double defaultRadius = 17.0;

//BUTTONTSTYLE
final primaryButton = ElevatedButton.styleFrom(
  primary: primaryColor,
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(defaultRadius),
  ),
);
